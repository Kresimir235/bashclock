A simple, but rather pretty, digital clock for the terminal, written as an exercise in bash scripting.

It works best in a fast terminal emulator (or in the TTY). 
